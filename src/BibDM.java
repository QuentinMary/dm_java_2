import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
	if (liste.isEmpty()){
		return null;
	}
        Integer minimum = liste.get(0);
	for (Integer elem:liste){
		if(elem < minimum){
			minimum = elem;
		}
	}
	return minimum;
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        if(liste.size()==0){
		return true;
    	}
	return valeur.compareTo(Collections.min(liste))<0;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
    	List<T> liste3 = new ArrayList<>();
	int a = 0;
	int b = 0;
	while (a < liste1.size() && b < liste2.size()){
		if (liste1.get(a).compareTo(liste2.get(b)) < 0){
			++a;
		}
		else if (liste1.get(a).compareTo(liste2.get(b)) > 0) {
			++b;
		}
		else{
			if(!liste3.contains(liste1.get(a)))
				liste3.add(liste1.get(a));
			++a;
			++b;
		}
	}
	return liste3;

    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> res = new ArrayList<>();
	String[] l = texte.split(" ");
	for(int i=0;i< l.length ; ++i){
		if(!l[i].equals("")){
			res.add(l[i]);
		}
	}
	return res;
     }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
	return null;
    }



    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        if(chaine.length() == 0) return true;
	boolean res = true;
	int ouvrante = 0;
	int fermante = 0;
	for(int i = 0; i<chaine.length(); ++i){
		if(chaine.charAt(i) == '(') ++ouvrante;
		if(chaine.charAt(i) == ')') ++fermante;
	}
	if(ouvrante != fermante) res = false;
	else if (chaine.charAt(0) != '(') res = false;
	else if (chaine.charAt(chaine.length()-1) != ')') res = false;
	return res;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        if(chaine.length() == 0){
          return true;
        }

        List<Boolean> sontParenthese = new ArrayList<Boolean>();
        for(int i = 0; i < chaine.length(); i++){
          if(chaine.charAt(i) == '('){
            sontParenthese.add(new Boolean(true));
          }

          else if(chaine.charAt(i) == '['){
            sontParenthese.add(new Boolean(false));
          }

          else if(chaine.charAt(i) == ')'){
            if(sontParenthese.isEmpty()){
              return false;
            }
            else if(sontParenthese.get(sontParenthese.size() - 1)){
              sontParenthese.remove(sontParenthese.size() - 1);
            }
            else{
              return false;
            }
          }

          else if(chaine.charAt(i) == ']'){
            if(sontParenthese.isEmpty()){
              return false;
            }
            else if(!sontParenthese.get(sontParenthese.size() - 1)){
              sontParenthese.remove(sontParenthese.size() - 1);
            }
            else{
              return false;
            }
          }

        }
        return sontParenthese.isEmpty();
    }




    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        if(liste.size()>0){
		int min = 0;
		int max = liste.size();
		int mid;
		while(min < max){
			mid = ((min + max)/2);
			if(valeur.compareTo(liste.get(mid))==0){
				return true;
			}
			else if(valeur.compareTo(liste.get(mid))>0){
				min = mid+1;
			}
			else{
				max = mid;
			}
		}
	}
	return false;
    }



}
